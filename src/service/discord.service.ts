import { Client, Events } from 'discord.js';
import CommandService from './command.service.js';
import EnvVarService from './envvar.service.js';

export default class DiscordService {
  private envVarService: EnvVarService;
  private commandService: CommandService;

  constructor(envVarService: EnvVarService, commandService: CommandService) {
    this.envVarService = envVarService;
    this.commandService = commandService;
  }

  public async init(): Promise<Client> {
    const client: Client = new Client({ intents: [] });

    client.once(Events.ClientReady, (c: Client<true>) => {
      console.log(`Ready! Logged in as ${c.user.tag}`);
    });

    await client.login(this.envVarService.getDiscordToken());

    process.once('SIGINT', () => {
      console.log('SIGINT');
      client.destroy();
    });
    process.once('SIGTERM', () => {
      console.log('SIGTERM');
      client.destroy();
    });

    return client;
  }
}
