import Daily from './Daily.js';
import Current from "./Current.js";
import Alert from "./Alert.js";
import Hourly from './Hourly';

export default interface WeatherReport {
    timezone: string;
    timezone_offset: string;
    current: Current;
    hourly: Hourly[];
    daily: Daily[],
    alerts: Alert[];
}
