import { DateTime } from 'luxon';

export default class DatetimeService {
  public static now(): DateTime {
    return DateTime.now();
  }
}
