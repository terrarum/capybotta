import Pollution from './Pollution';

export default interface PollutionReport {
  coord: {
    lon: number;
    lat: number;
  }
  list: Pollution[]
}
