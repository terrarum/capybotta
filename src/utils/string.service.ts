export default class StringService {
  // a function that converts camel case to title case
  public static camelToTitleCase(camelCase: string): string {
    return camelCase.replace(/([A-Z])/g, ' $1').replace(/^./, (str: string) => str.toUpperCase());
  }
}
