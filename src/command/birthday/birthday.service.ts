import * as Sentry from '@sentry/node';
import { ChatInputCommandInteraction, ModalSubmitInteraction, userMention } from 'discord.js';
import { DateTime } from 'luxon';
import DatetimeService from '../../service/datetime.service';
import EnvVarService from '../../service/envvar.service';
import OutputService from './output.service';
import QueryService from './query.service';
import ValidationService from './validation.service';
import Birthday from '../../model/birthday';

export default class BirthdayService {
  private envVarService: EnvVarService;
  private queryService: QueryService;

  constructor(envVarService: EnvVarService, queryService: QueryService) {
    this.envVarService = envVarService;
    this.queryService = queryService;
  }

  public async list(interaction: ChatInputCommandInteraction): Promise<string> {
    if (!interaction.inGuild()) {
      return 'This command must be used in a server.';
    }

    const serverId: string = interaction.guildId;

    let birthdays: Birthday[];
    try {
      birthdays = await this.queryService.getBirthdays(serverId);
    } catch (e) {
      Sentry.captureException(e);
      return 'Could not list birthdays. Please try again.';
    }

    if (birthdays.length === 0) {
      return 'No birthdays set.';
    }

    return OutputService.list(DatetimeService.now(), birthdays);
  }

  public async remove(interaction: ChatInputCommandInteraction): Promise<string> {
    if (!interaction.inGuild()) {
      return 'This command must be used in a server.';
    }

    const userId: string = interaction.user.id;
    const serverId: string = interaction.guildId;

    try {
      const birthday: Birthday | undefined = await this.queryService.getBirthday(userId, serverId);
      if (!birthday) {
        return `No birthday set for ${userMention(userId)}.`;
      }
      await this.queryService.removeBirthday(userId, serverId);
    } catch (e) {
      Sentry.captureException(e);
      return 'Could not remove birthday. Please try again.';
    }

    const mention: string = userMention(userId);
    return `Removed birthday for ${mention}.`;
  }

  public async set(interaction: ModalSubmitInteraction): Promise<string> {
    if (!interaction.inGuild()) {
      return 'This command must be used in a server.';
    }

    const userId: string = interaction.user.id;
    const serverId: string = interaction.guildId;
    const dayInput: string = interaction.fields.getTextInputValue('birthdaySetDay');
    const monthInput: string = interaction.fields.getTextInputValue('birthdaySetMonth');
    const yearInput: string = interaction.fields.getTextInputValue('birthdaySetYear');
    const timezoneInput: string = interaction.fields.getTextInputValue('birthdaySetTimezone');

    const timezone: string = timezoneInput === '' ? this.envVarService.getDefaultTimezone() : timezoneInput;

    if (!ValidationService.validate(dayInput, monthInput, yearInput, timezone)) {
      return 'Invalid date.';
    }

    const day: number = parseInt(dayInput);
    const month: number = parseInt(monthInput);
    const year: number | null = yearInput === '' ? null : parseInt(yearInput);

    try {
      await this.queryService.setBirthday(userId, serverId, day, month, year, timezone);
    } catch (e) {
      Sentry.captureException(e);
      return 'Could not set birthday. Please try again.';
    }

    const mention: string = userMention(userId);

    const dateTime: DateTime = DateTime.local(year ?? 2000, month, day).setLocale('en-AU');

    const dateString: string =
      yearInput === ''
        ? dateTime.toLocaleString({ month: 'long', day: 'numeric' })
        : dateTime.toLocaleString({ month: 'long', day: 'numeric', year: 'numeric' });

    return `Setting ${mention}'s birthday to ${dateString}.`;
  }
}
