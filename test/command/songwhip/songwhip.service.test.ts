import SongwhipService from '../../../src/command/songwhip/songwhip.service.js';
import { Artist, MusicServiceLink, SongwhipDto } from '../../../src/model/songwhipDto.js';
import SongLink from '../../../src/model/songlink.js';
import FetchService from '../../../src/utils/fetch.service.js';
import { beforeEach, describe, expect, it, vi } from 'vitest';

vi.mock('../../../src/utils/fetch.service.js');

describe('SongwhipService', () => {
    let fetchService: FetchService;
    let songwhipService: SongwhipService;

    beforeEach(() => {
        fetchService = new FetchService();
        songwhipService = new SongwhipService(fetchService);
    });

    it('should return an error if the request fails', async () => {
        fetchService.post.mockRejectedValue('ERROR');

        await expect(songwhipService.getSong('https://open.spotify.com/track/3sdafLAGAN83iudA3YEP39Xgkr'))
            .rejects
            .toThrowError('Could not get song data: ERROR');
    });

    it.each([
        [
            [
                {
                    name: 'Artist 1'
                },
                {
                    name: 'Artist 2'
                }
            ],
            'Artist 1, Artist 2'
        ],
        [
            [
                {
                    name: 'Artist 1'
                }
            ],
            'Artist 1'
        ]
    ])('should get the artists from the songwhip response', (artists: Artist[], expected) => {
        const songwhipResponse: SongwhipDto = {
            status: '',
            data: {
                item: {
                    artists,
                    type: '',
                    name: '',
                    url: '',
                    image: '',
                    linksCountries: [],
                    links: {}
                }
            }
        };

        expect(songwhipService.getArtists(songwhipResponse)).toEqual(expected);
    });

    it.each([
        [
            {
                'spotify': [
                    {
                        'link': 'https://open.spotify.com/track/3LAGAN83iudA3YEP39Xgkr',
                        'countries': ['AU', 'NZ']
                    }
                ],
            },
            {
                serviceName: 'spotify',
                displayName: 'Spotify',
                link: 'https://open.spotify.com/track/3LAGAN83iudA3YEP39Xgkr',
            }
        ],
        [
            {
                "youtube": [
                    {
                        "link": "https://youtube.com/watch?v=B3sn30XQhL4",
                        "countries": null
                    }
                ]
            },
            {
                serviceName: 'youtube',
                displayName: 'YouTube',
                link: 'https://youtube.com/watch?v=B3sn30XQhL4',
            }
        ],
        [
            {
                "youtubeMusic": [
                    {
                        "link": "https://music.youtube.com/watch?v=B3sn30XQhL4",
                        "countries": null
                    }
                ]
            },
            {
                serviceName: 'youtubeMusic',
                displayName: 'YouTube Music',
                link: 'https://music.youtube.com/watch?v=B3sn30XQhL4',
            }
        ],
        [
            {
                "itunes": [
                    {
                        "link": "https://music.apple.com/{country}/album/halo/1686018864?i=1686018867&app=music",
                        "countries": [
                            "AU"
                        ]
                    }
                ]
            },
            {
                serviceName: 'itunes',
                displayName: 'iTunes',
                link: 'https://music.apple.com/au/album/halo/1686018864?i=1686018867&app=music',
            }
        ],
        [
            {
                "itunesStore": [
                    {
                        "link": "https://music.apple.com/{country}/album/halo/1686018864?i=1686018867&app=itunes",
                        "countries": [
                            "AU"
                        ]
                    }
                ]
            },
            {
                serviceName: 'itunesStore',
                displayName: 'iTunes Store',
                link: 'https://music.apple.com/au/album/halo/1686018864?i=1686018867&app=itunes',
            }
        ],
        [
            {
                "qobuz": [
                    {
                        "link": "https://open.qobuz.com/track/212795987",
                        "countries": null
                    }
                ]
            },
            {
                serviceName: 'qobuz',
                displayName: 'Qobuz',
                link: 'https://open.qobuz.com/track/212795987',
            }
        ],
        [
            {
                "tidal": [
                    {
                        "link": "https://listen.tidal.com/track/292641694",
                        "countries": [
                            "AU"
                        ]
                    }
                ],
            },
            {
                serviceName: 'tidal',
                displayName: 'Tidal',
                link: 'https://listen.tidal.com/track/292641694',
            }
        ],
        [
            {
                "amazon": [
                    {
                        "link": "https://amazon.com/dp/B0C6D1FH55",
                        "countries": [
                            "US"
                        ]
                    }
                ],
            },
            {
                serviceName: 'amazon',
                displayName: 'Amazon',
                link: 'https://amazon.com/dp/B0C6D1FH55',
            }
        ],
        [
            {
                "deezer": [
                    {
                        "link": "https://www.deezer.com/track/2300221655",
                        "countries": null
                    }
                ],
            },
            {
                serviceName: 'deezer',
                displayName: 'Deezer',
                link: 'https://www.deezer.com/track/2300221655',
            }
        ],
        [
            {
                "napster": [
                    {
                        "link": "https://play.napster.com/artist/art.8930712?trackId=tra.777632455",
                        "countries": null
                    }
                ],
            },
            {
                serviceName: 'napster',
                displayName: 'Napster',
                link: 'https://play.napster.com/artist/art.8930712?trackId=tra.777632455',
            }
        ],
        [
            {
                "audiomack": [
                    {
                        "link": "https://audiomack.com/song/pendulum/halo",
                        "countries": null
                    }
                ],
            },
            {
                serviceName: 'audiomack',
                displayName: 'Audiomack',
                link: 'https://audiomack.com/song/pendulum/halo',
            }
        ],
        [
            {
                "lineMusic": [
                    {
                        "link": "https://music.line.me/webapp/track/mt000000001ac7e133",
                        "countries": null
                    }
                ],
            },
            {
                serviceName: 'lineMusic',
                displayName: 'Line Music',
                link: 'https://music.line.me/webapp/track/mt000000001ac7e133',
            }
        ],
        [
            {
                "amazonMusic": [
                    {
                        "link": "https://music.amazon.com/albums/B0C6D4KFK6?trackAsin=B0C6D1FH55",
                        "countries": [
                            "US"
                        ]
                    }
                ],
            },
            {
                serviceName: 'amazonMusic',
                displayName: 'Amazon Music',
                link: 'https://music.amazon.com/albums/B0C6D4KFK6?trackAsin=B0C6D1FH55',
            }
        ],
    ])('should map %s to %s', (input: { [p: string]: MusicServiceLink[] }, expected: SongLink) => {
        const actual = songwhipService.getSongLinks(input)[0];
        expect(actual).toEqual(expected);
    });
});
