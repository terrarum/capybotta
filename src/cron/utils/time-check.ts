import { DateTime } from 'luxon';

export default class TimeCheck {
  public isHour(hour: number, timezone: string): boolean {
    const now = DateTime.now().setZone(timezone);
    return now.hour === hour;
  }
}
