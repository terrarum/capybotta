/**
 * Any random method should be added here so that it can be mocked easily in unit tests.
 */
export default class RandomService {
  public static getRandomInt(max: number): number {
    return Math.floor(Math.random() * max);
  }

  public static randRange(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
}
