import { userMention } from 'discord.js';
import { DateTime } from 'luxon';
import Birthday from '../../model/birthday';

export default class OutputService {
  private static sortBirthdays(now: DateTime, birthdays: Birthday[]): Birthday[] {
    const sortedBirthdays: Birthday[] = birthdays.sort((a, b) => {
      const aDate: DateTime = DateTime.fromObject({ day: a.day, month: a.month, year: now.year });
      const bDate: DateTime = DateTime.fromObject({ day: b.day, month: b.month, year: now.year });

      const aDiff: number = aDate.diff(now, 'milliseconds').milliseconds;
      const bDiff: number = bDate.diff(now, 'milliseconds').milliseconds;

      return aDiff - bDiff;
    });

    const upcomingBirthdays: Birthday[] = sortedBirthdays.filter((birthday) => {
      const date: DateTime = DateTime.fromObject({ day: birthday.day, month: birthday.month, year: now.year });
      return date.diff(now, 'milliseconds').milliseconds >= 0;
    });

    const pastBirthdays: Birthday[] = sortedBirthdays.filter((birthday) => {
      const date: DateTime = DateTime.fromObject({ day: birthday.day, month: birthday.month, year: now.year });
      return date.diff(now, 'milliseconds').milliseconds < 0;
    });

    return upcomingBirthdays.concat(pastBirthdays);
  }

  private static getDaysAway(now: DateTime, date: DateTime): number {
    const dayDiff: number = date.diff(now, 'days').days;
    return dayDiff >= 0 ? dayDiff : 365 + dayDiff;
  }

  private static getDaysUntilNextYear(now: DateTime): number {
    const nextYear = DateTime.fromObject({ day: 1, month: 1, year: now.year + 1 });
    return nextYear.diff(now, 'days').days;
  }

  private static getAge(now: DateTime, birthday: Birthday, daysUntilNextYear: number, daysAway: number): number | null {
    const { day, month, year } = birthday;
    if (year === null) return null;
    const birthdayDate = DateTime.fromObject({ day, month, year });

    if (daysAway >= daysUntilNextYear) return now.year - birthdayDate.year + 1;
    return now.year - birthdayDate.year;
  }

  public static list(nowRaw: DateTime, birthdays: Birthday[]): string {
    let yearDivided = false;
    const now = DateTime.fromObject({ day: nowRaw.day, month: nowRaw.month, year: nowRaw.year });

    const daysUntilNextYear: number = this.getDaysUntilNextYear(now);

    const ages: number[] = [];

    const birthdayList: string = this.sortBirthdays(now, birthdays)
      .map((birthday) => {
        const { discord_id, day, month } = birthday;

        const birthdayDate: DateTime = DateTime.fromObject({ day, month, year: nowRaw.year });
        const daysAway: number = this.getDaysAway(now, birthdayDate);

        let yearDivider: string = '';
        if (!yearDivided && daysAway >= daysUntilNextYear) {
          yearDivider = `----- ${now.year + 1}\n`;
          yearDivided = true;
        }

        const dateMessage = ` - ${birthdayDate.toFormat('dd MMMM')}`;
        const daysAwayMessage =
          daysAway === 0
            ? ' - Today!'
            : ` - ${daysAway} ${daysAway === 1 ? 'day' : 'days'} away${daysAway < 7 ? '!' : ''}`;

        const age = this.getAge(now, birthday, daysUntilNextYear, daysAway);
        if (age !== null) ages.push(age);
        const ageMessage: string = birthday.year === null ? '' : ` - ${age} years old`;

        return `${yearDivider}${userMention(discord_id)}${dateMessage}${daysAwayMessage}${ageMessage}`;
      })
      .join('\n');

    const averageAge: number = ages.reduce((a, b) => a + b - 1, 0) / ages.length;
    const averageAgeMessage: string = `Average age is ${averageAge.toFixed(1)}`;
    return `Birthdays in Date Order:\n${birthdayList}${ages.length > 0 ? `\n${averageAgeMessage}` : ''}`;
  }
}
