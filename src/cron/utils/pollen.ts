import { JSDOM } from 'jsdom';

export async function getPollenLevel(): Promise<string> {
  const data = await fetch('https://www.melbournepollen.com.au/');
  const text = await data.text();
  const { document } = new JSDOM(text).window;

  const pLevel: Element | null = document.querySelector('#plevel');

  if (!pLevel || !pLevel.textContent) {
    return 'unknown';
  }

  return pLevel.textContent.trim().toLowerCase();
}
