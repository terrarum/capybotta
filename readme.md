# Capybotta

A Discord chat bot.

## Build Process
Local dev uses ts-node to avoid a compilation step. Deployment uses `tsc` to build the bot using the `Node16` typescript module syntax. This does not build the entire app into a single file/directory and so the full build process needs to install npm packages as well.

Building the entire application into a single file can be accomplished with webpack but this will do for now.

## Adding New Commands
1. Create a class in `src/commands` that implements `Command`.
2. Add the class to the `commands` property in `src/service/command.service.ts`.

## Running the Bot Locally
1. Go to https://discord.com/developers/applications and create a new application.
   1. Name it something like `capybotta-local-YOURNAME`
2. Create a .env file in the root dir (same as `package.json`) with the following variables:
   1. `DISCORD_TOKEN` - Bot > Token > Click Reset Token
   2. `DISCORD_CLIENT_ID` - OAuth2 > General > Client Information > Client ID
   3. `DATABASE_URL` - The SQLite connection string for the local database
   4. `ANNOUNCEMENT_CHANNEL_ID` - The ID of the channel to send general messages to
   5. `BIRTHDAY_CHANNEL_ID` - The ID of the channel to send birthday messages to
   6. `SENTRY_DSN` - The Sentry DSN for error reporting (not required to run locally so a dummy value will do)
      ```
      DISCORD_TOKEN=abc123
      DISCORD_CLIENT_ID=123456
      TURSO_CONNECTION_URL=http://localhost:3000
      TURSO_AUTH_TOKEN=abc123
      ANNOUNCEMENT_CHANNEL_ID=123456
      BIRTHDAY_CHANNEL_ID=123456
      SENTRY_DSN=local
      ```
3. Create a Discord Server to test the bot in if you don't have an existing appropriate server.
4. Add the bot to your Discord Server
   1. OAuth2 > URL Generator
   2. Select `bot` and `applications.commands`
   3. Select any other scopes or permissions you need for your new feature
   4. Copy URL > Paste into browser > Select server
5. Run `npm run dev` to start the bot

# Running a Cron Task Locally
`ts-node src/cron/birthday.ts` will run the birthday cron task without having to build to js first.

# Unit Tests
Tests are written with Vitest. 

# Weather
- Weather: https://openweathermap.org/api/one-call-3
- Pollution: https://openweathermap.org/api/air-pollution
