import * as Sentry from '@sentry/node';
import { Client } from 'discord.js';
import { DateTime } from 'luxon';
import QueryService from '../command/birthday/query.service';
import EnvVarService from '../service/envvar.service';
import BirthdayUpdateService from './birthday-update.service';
import { drizzle, LibSQLDatabase } from 'drizzle-orm/libsql';
import TimeCheck from './utils/time-check';
import VicDatesService from './vic-dates.service';
import WeatherService from '../service/weather.service';
import GreetingService from '../service/greeting.service';
import MorningUpdateService from './morning-update.service';

const timeCheck: TimeCheck = new TimeCheck();

if (process.argv[2] !== 'test' && !timeCheck.isHour(6, 'Australia/Melbourne')) {
  process.exit(0);
}

const envVarService: EnvVarService = new EnvVarService();

Sentry.init({
  dsn: envVarService.getSentryDsn(),
  tracesSampleRate: 1.0,
  profilesSampleRate: 1.0,
});

const discordClient: Client = new Client({ intents: [] });

const databaseClient: LibSQLDatabase = drizzle({ connection: {
    url: envVarService.getTursoConnectionUrl(),
    authToken: envVarService.getTursoAuthToken(),
  }});

const queryService: QueryService = new QueryService(databaseClient);

// Birthday Service.
const birthdayUpdateService: BirthdayUpdateService = new BirthdayUpdateService(
  queryService,
  envVarService,
  discordClient,
);

// Victorian Dates Service.
const vicDatesService = new VicDatesService(envVarService, discordClient);

// Morning Update Service.
const weatherService: WeatherService = new WeatherService(envVarService);
const greetingService: GreetingService = new GreetingService();
const morningUpdateService = new MorningUpdateService(envVarService, discordClient, weatherService, greetingService);

const now: DateTime = DateTime.now().setZone('Australia/Melbourne');

async function init() {
  try {
    await discordClient.login(envVarService.getDiscordToken());
  } catch (e) {
    Sentry.captureException(e);
    console.error(e);
  }

  try {
    await morningUpdateService.update();
  } catch (e) {
    Sentry.captureException(e);
    console.error(e);
  }

  try {
    await birthdayUpdateService.update(now);
  } catch (e) {
    Sentry.captureException(e);
    console.error(e);
  }

  try {
    await vicDatesService.update(now);
  } catch (e) {
    Sentry.captureException(e);
    console.error(e);
  }

  process.exit(0);
}

init().then();
