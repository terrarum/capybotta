import { REST, Routes } from 'discord.js';
import Birthday from '../command/birthday';
import BirthdayService from '../command/birthday/birthday.service';
import Songwhip from '../command/songwhip.js';
import SongwhipService from '../command/songwhip/songwhip.service.js';
import Which from '../command/which.js';
import Command from '../interface/command.js';
import EnvVarService from './envvar.service.js';

export default class CommandService {
  private envVarService: EnvVarService;
  private readonly songwhipService: SongwhipService;
  private readonly birthdayService: BirthdayService;

  constructor(envVarService: EnvVarService, songwhipService: SongwhipService, birthdayService: BirthdayService) {
    this.envVarService = envVarService;
    this.songwhipService = songwhipService;
    this.birthdayService = birthdayService;
  }

  public getCommands(): Command[] {
    return [new Which(), new Songwhip(this.songwhipService), new Birthday(this.birthdayService)];
  }

  public async registerCommands(commands: Command[]): Promise<void> {
    const rest: REST = new REST().setToken(this.envVarService.getDiscordToken());

    const body = commands.map((command: Command) => command.data.toJSON());

    await rest.put(Routes.applicationCommands(this.envVarService.getDiscordClientId()), {
      body,
    });
  }
}
