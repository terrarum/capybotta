export default interface SongLink {
  serviceName: string;
  displayName: string;
  link: string;
}
