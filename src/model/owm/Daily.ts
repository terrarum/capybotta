import WeatherDescription from "./WeatherDescription.js";

export default interface Daily {
    dt: number;
    sunrise: number;
    sunset: number;
    moonrise: number;
    moonset: number;
    moon_phase: number;
    summary: string;
    pressure: number;
    humidity: number;
    dew_point: number;
    wind_speed: number;
    wind_deg: number;
    wind_gust: number;
    clouds: number;
    pop: number;
    rain?: number;
    snow?: number;
    uvi: number;
    temp: {
        day: number;
        min: number;
        max: number;
        night: number;
        eve: number;
        morn: number;
    },
    feels_like: {
        day: number;    // 1200
        night: number;  // 0000
        eve: number;    // 1800
        morn: number;   // 0600
    },
    weather: WeatherDescription[];
}
