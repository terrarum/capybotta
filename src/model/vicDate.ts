export enum VicDateType {
  PUBLIC_HOLIDAY = 'PUBLIC_HOLIDAY',
  DAYLIGHT_SAVING = 'DAYLIGHT_SAVING',
}

export default interface VicDate {
  uuid: string;
  name: string;
  type: VicDateType;
  date: string;
  description: string;
}
