import * as Sentry from '@sentry/node';
import SongLink from '../../model/songlink.js';
import { MusicServiceLink, SongwhipDto } from '../../model/songwhipDto.js';
import FetchService from '../../utils/fetch.service.js';
import StringService from '../../utils/string.service.js';

export default class SongwhipService {
  private fetchService: FetchService;
  private static readonly songwhipUrl: string = 'https://songwhip.com/api/songwhip/create';

  constructor(fetchService: FetchService) {
    this.fetchService = fetchService;
  }

  public async getSong(link: string): Promise<SongwhipDto> {
    try {
      return await this.fetchService.post<SongwhipDto>(
        SongwhipService.songwhipUrl,
        JSON.stringify({
          url: link,
          country: 'AU',
        }),
      );
    } catch (e) {
      Sentry.captureException(e);
      throw new Error(`Could not get song data: ${e}`);
    }
  }

  public getArtists(songwhip: SongwhipDto): string {
    return songwhip.data.item.artists.map((artist) => artist.name).join(', ');
  }

  public getSongLinks(serviceLinks: { [p: string]: MusicServiceLink[] }): SongLink[] {
    const songLinks: SongLink[] = [];

    for (const [service, musicServiceLink] of Object.entries(serviceLinks)) {
      const musicService = musicServiceLink[0];

      if (!musicService) {
        continue;
      }

      songLinks.push({
        serviceName: service,
        displayName: this.serviceNameProcessor(service),
        link: musicService.link,
      });
    }

    // for each songlink, replace {country} in the link with 'au'
    songLinks.forEach((songLink) => {
      songLink.link = songLink.link.replace('{country}', 'au');
    });

    return songLinks;
  }

  private serviceNameProcessor(serviceName: string): string {
    switch (serviceName) {
      case 'youtube':
        return 'YouTube';
      case 'youtubeMusic':
        return 'YouTube Music';
      case 'itunes':
        return 'iTunes';
      case 'itunesStore':
        return 'iTunes Store';
      default:
        return StringService.camelToTitleCase(serviceName);
    }
  }
}
