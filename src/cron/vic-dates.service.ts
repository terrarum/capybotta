import { Client, TextChannel } from 'discord.js';
import { DateTime } from 'luxon';
import EnvVarService from '../service/envvar.service';
import VicDate, { VicDateType } from '../model/vicDate';

export default class VicDatesService {
  envVarService: EnvVarService;
  discordClient: Client;

  constructor(envVarService: EnvVarService, discordClient: Client) {
    this.envVarService = envVarService;
    this.discordClient = discordClient;
  }

  private async getDates(from: string, to: string): Promise<VicDate[]> {
    const apiKey: string = this.envVarService.getVicApiKey();

    const url: string = 'https://wovg-community.gateway.prod.api.vic.gov.au/vicgov/v2.0/dates';

    const types: string[] = ['PUBLIC_HOLIDAY', 'DAYLIGHT_SAVING'];

    const queryParams: URLSearchParams = new URLSearchParams();
    queryParams.append('from_date', from);
    queryParams.append('to_date', to);
    queryParams.append('type', types.join(','));
    queryParams.append('sort', 'date:asc');

    const response = await fetch(`${url}?${queryParams.toString()}`, {
      headers: {
        'apikey': apiKey,
      },
    });

    if (!response.ok) {
      throw new Error(`Failed to fetch VIC dates: ${response.statusText}`);
    }

    const data = await response.json();
    return data.dates;
  }

  private formatMessage(timeInMelbourne: DateTime, dates: VicDate[]): string {
    const messages: string[] = dates.map((date) => {
      const eventDate: DateTime = DateTime.fromISO(date.date, {zone: 'Australia/Melbourne'});
      const diff: number = Math.ceil(eventDate.diff(timeInMelbourne, 'days').days);
      const days: string = diff === 1 ? 'day' : 'days';
      const join: string = date.type === VicDateType.DAYLIGHT_SAVING ? 'in' : 'is in';
      return `${date.name} ${join} ${diff} ${days}.`;
    });

    return messages.join('\n');
  }

  private async sendMessage(message: string): Promise<void> {
    const channel: TextChannel = (await this.discordClient.channels.fetch(
      this.envVarService.getAnnouncementChannelId(),
    )) as TextChannel;
    await channel.send(message);
  }

  async update(timeInMelbourne: DateTime): Promise<void> {
    const from: string | null = timeInMelbourne.toISODate();
    const to: string | null  = timeInMelbourne.plus({ days: 30 }).toISODate();

    if (from === null || to === null) {
      throw new Error('Invalid date');
    }

    const dates: VicDate[] = await this.getDates(from, to);

    // Remind people about events in 1, 7 and 30 days
    const reminders: VicDate[] = dates.filter((date) => {
      const eventDate: DateTime = DateTime.fromISO(date.date, {zone: 'Australia/Melbourne'});
      const diff: number = Math.ceil(eventDate.diff(timeInMelbourne, 'days').days);
      return [1, 7, 30].includes(diff);
    });

    const message: string = this.formatMessage(timeInMelbourne, reminders);

    if (message) {
      await this.sendMessage(message);
    }
  }
}
