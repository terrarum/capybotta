export default class FetchService {
  public async post<T>(url: string, body: string): Promise<T> {
    const response: Response | void = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body,
    });

    if (response) {
      return await response.json();
    }

    return Promise.reject('Error fetching data');
  }
}
