import WeatherDescription from './WeatherDescription';

export default interface Hourly {
    dt: number;
    temp: number;
    feels_like: number;
    pressure: number;
    humidity: number;
    dew_point: number;
    uvi: number;
    clouds: number;
    visibility: number;
    wind_speed: number;
    wind_deg: number;
    wind_gust: number;
    weather: WeatherDescription[];
    pop: number;
    rain?: {
      '1h': number;
    };
    snow?: {
      '1h': number;
    };
}
