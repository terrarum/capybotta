CREATE TABLE `birthday` (
	`id` integer PRIMARY KEY AUTOINCREMENT NOT NULL,
	`discord_id` text NOT NULL,
	`server_id` text NOT NULL,
	`day` integer NOT NULL,
	`month` integer NOT NULL,
	`year` integer,
	`timezone` text NOT NULL,
	`updated_on` integer DEFAULT CURRENT_TIMESTAMP
);
--> statement-breakpoint
CREATE UNIQUE INDEX `birthday_server_user_index` ON `birthday` (`discord_id`,`server_id`);