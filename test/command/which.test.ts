import Which from '../../src/command/which.js';
import { ChatInputCommandInteraction } from 'discord.js';
import RandomService from '../../src/utils/random.service';
import { beforeEach, describe, expect, it, vi } from 'vitest';

vi.mock('../../src/utils/random.service');

describe('Which', () => {
    let command: Which

    beforeEach(() => {
        command = new Which();
    });

    it.each([
        ['a b c', 'a'],
        ['a,b,c', 'a'],
    ])('should return a random option from a list of options', async (input: string, expected: string) => {
        RandomService.getRandomInt = vi.fn().mockReturnValue(0);

        const interaction = {
          options: {
            getString: vi.fn().mockReturnValue(input),
          },
          reply: vi.fn(),
        } as unknown as ChatInputCommandInteraction;

        await command.execute(interaction);

        expect(interaction.reply).toHaveBeenCalledWith(`From \`${input}\`\nI choose \`${expected}\``);
    });
});
