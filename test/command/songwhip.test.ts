import Songwhip from '../../src/command/songwhip.js';
import SongwhipService from '../../src/command/songwhip/songwhip.service.js';
import FetchService from '../../src/utils/fetch.service.js';
import { ActionRowBuilder, ButtonBuilder, ButtonStyle, ChatInputCommandInteraction } from 'discord.js';
import { SongwhipDto } from '../../src/model/songwhipDto.js';
import { beforeEach, describe, expect, it, vi } from 'vitest';

vi.mock('../../src/utils/fetch.service.js');

describe('Songwhip', () => {
    let fetchService: FetchService;
    let songwhipService: SongwhipService;
    let command: Songwhip;

    beforeEach(() => {
        fetchService = new FetchService();
        songwhipService = new SongwhipService(fetchService);
        command = new Songwhip(songwhipService);
    });

    it('should defer reply', async () => {
        const searchTerm = 'https://www.youtube.com/watch?v=somevideo';

        const response: SongwhipDto = {
            status: 'success',
            data: {
                item: {
                    type: '',
                    name: 'Song Name',
                    url: '',
                    image: 'https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/0e/db/9d/0edb9da2-64dc-87f8-3f93-6c3a3a0410ca/7267.jpg/1400x1400bb.jpg',
                    linksCountries: [],
                    links: {
                        spotify: [{
                            link: 'https://open.spotify.com/track/3LAGAN83iudA3YEP39Xgkr',
                            countries: null,
                        }]
                    },
                    artists: [{
                        name: 'Artist'
                    }]
                }
            },
        };

        fetchService.post.mockReturnValue(Promise.resolve(response));

        const interaction = {
          options: {
            getString: vi.fn().mockReturnValue(searchTerm),
          },
          editReply: vi.fn(),
          deferReply: vi.fn(),
        } as unknown as ChatInputCommandInteraction;

        await command.execute(interaction);

        const buttonRows: ActionRowBuilder<ButtonBuilder>[] = [
            new ActionRowBuilder<ButtonBuilder>().addComponents(
                new ButtonBuilder()
                    .setStyle(ButtonStyle.Link)
                    .setLabel('Spotify')
                    .setURL('https://open.spotify.com/track/3LAGAN83iudA3YEP39Xgkr')
            )
        ];

        expect(interaction.deferReply).toHaveBeenCalled();
        expect(interaction.editReply).toHaveBeenCalledWith({
            content: `You searched for ${searchTerm}`,
            components: buttonRows,
            embeds: [{
                title: 'Song Name by Artist',
                image: {
                    url: 'https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/0e/db/9d/0edb9da2-64dc-87f8-3f93-6c3a3a0410ca/7267.jpg/1400x1400bb.jpg',
                }
            }],
        });
    });

    it('should return a songwhip link', async () => {
        const searchTerm = 'https://www.youtube.com/watch?v=somevideo';

        const response: SongwhipDto = {
            status: 'success',
            data: {
                item: {
                    type: '',
                    name: 'Song Name',
                    url: '',
                    image: 'https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/0e/db/9d/0edb9da2-64dc-87f8-3f93-6c3a3a0410ca/7267.jpg/1400x1400bb.jpg',
                    linksCountries: [],
                    links: {
                        spotify: [{
                            link: 'https://open.spotify.com/track/3LAGAN83iudA3YEP39Xgkr',
                            countries: null,
                        }]
                    },
                    artists: [{
                        name: 'Artist'
                    }]
                }
            },
        };

        fetchService.post.mockReturnValue(Promise.resolve(response));

        const interaction = {
          options: {
            getString: vi.fn().mockReturnValue(searchTerm),
          },
          editReply: vi.fn(),
          deferReply: vi.fn(),
        } as unknown as ChatInputCommandInteraction;

        await command.execute(interaction);

        const buttonRows: ActionRowBuilder<ButtonBuilder>[] = [
            new ActionRowBuilder<ButtonBuilder>().addComponents(
                new ButtonBuilder()
                    .setStyle(ButtonStyle.Link)
                    .setLabel('Spotify')
                    .setURL('https://open.spotify.com/track/3LAGAN83iudA3YEP39Xgkr')
            )
        ];
        expect(interaction.editReply).toHaveBeenCalledWith({
            content: `You searched for ${searchTerm}`,
            components: buttonRows,
            embeds: [{
                title: 'Song Name by Artist',
                image: {
                    url: 'https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/0e/db/9d/0edb9da2-64dc-87f8-3f93-6c3a3a0410ca/7267.jpg/1400x1400bb.jpg',
                }
            }],
        });
    });

    it('should return songwhip links in rows of 5', async () => {
        const searchTerm = 'https://www.youtube.com/watch?v=somevideo';

        const response: SongwhipDto = {
            status: 'success',
            data: {
                item: {
                    type: '',
                    name: 'Song Name',
                    url: '',
                    image: 'https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/0e/db/9d/0edb9da2-64dc-87f8-3f93-6c3a3a0410ca/7267.jpg/1400x1400bb.jpg',
                    linksCountries: [],
                    links: {
                        spotify1: [{
                            link: 'https://open.spotify.com/track/3LAGAN83iudA3YEP39Xgkr',
                            countries: null,
                        }],
                        spotify2: [{
                            link: 'https://open.spotify.com/track/3LAGAN83iudA3YEP39Xgkr',
                            countries: null,
                        }],
                        spotify3: [{
                            link: 'https://open.spotify.com/track/3LAGAN83iudA3YEP39Xgkr',
                            countries: null,
                        }],
                        spotify4: [{
                            link: 'https://open.spotify.com/track/3LAGAN83iudA3YEP39Xgkr',
                            countries: null,
                        }],
                        spotify5: [{
                            link: 'https://open.spotify.com/track/3LAGAN83iudA3YEP39Xgkr',
                            countries: null,
                        }],
                        spotify6: [{
                            link: 'https://open.spotify.com/track/3LAGAN83iudA3YEP39Xgkr',
                            countries: null,
                        }],
                    },
                    artists: [{
                        name: 'Artist'
                    }]
                }
            },
        };

        fetchService.post.mockReturnValue(Promise.resolve(response));

        const interaction = {
          options: {
            getString: vi.fn().mockReturnValue(searchTerm),
          },
          editReply: vi.fn(),
          deferReply: vi.fn(),
        } as unknown as ChatInputCommandInteraction;

        await command.execute(interaction);

        const buttonRows: ActionRowBuilder<ButtonBuilder>[] = [
            new ActionRowBuilder<ButtonBuilder>().addComponents(
                new ButtonBuilder()
                    .setStyle(ButtonStyle.Link)
                    .setLabel('Spotify1')
                    .setURL('https://open.spotify.com/track/3LAGAN83iudA3YEP39Xgkr'),
                new ButtonBuilder()
                    .setStyle(ButtonStyle.Link)
                    .setLabel('Spotify2')
                    .setURL('https://open.spotify.com/track/3LAGAN83iudA3YEP39Xgkr'),
                new ButtonBuilder()
                    .setStyle(ButtonStyle.Link)
                    .setLabel('Spotify3')
                    .setURL('https://open.spotify.com/track/3LAGAN83iudA3YEP39Xgkr'),
                new ButtonBuilder()
                    .setStyle(ButtonStyle.Link)
                    .setLabel('Spotify4')
                    .setURL('https://open.spotify.com/track/3LAGAN83iudA3YEP39Xgkr'),
                new ButtonBuilder()
                    .setStyle(ButtonStyle.Link)
                    .setLabel('Spotify5')
                    .setURL('https://open.spotify.com/track/3LAGAN83iudA3YEP39Xgkr'),
            ),
            new ActionRowBuilder<ButtonBuilder>().addComponents(
                new ButtonBuilder()
                    .setStyle(ButtonStyle.Link)
                    .setLabel('Spotify6')
                    .setURL('https://open.spotify.com/track/3LAGAN83iudA3YEP39Xgkr'),
            ),
        ];

        expect(interaction.editReply).toHaveBeenCalledWith({
            content: `You searched for ${searchTerm}`,
            components: buttonRows,
            embeds: [{
                title: 'Song Name by Artist',
                image: {
                    url: 'https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/0e/db/9d/0edb9da2-64dc-87f8-3f93-6c3a3a0410ca/7267.jpg/1400x1400bb.jpg',
                }
            }],
        });
    });
});
