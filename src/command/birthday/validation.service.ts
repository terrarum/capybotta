import { DateTime } from 'luxon';

export default class ValidationService {
  // Just to make Luxon happy for date validation.
  private static ARBITRARY_YEAR: number = 2000;

  public static validate(day: string, month: string, year: string, timezone: string): boolean {
    const yearInt = year === '' ? this.ARBITRARY_YEAR : parseInt(year);
    const monthInt = parseInt(month);
    const dayInt = parseInt(day);

    const date = DateTime.local(yearInt, monthInt, dayInt).setZone(timezone);

    if (!date.isValid) {
      return false;
    }

    return date.diffNow('millisecond').milliseconds <= 0;
  }
}
