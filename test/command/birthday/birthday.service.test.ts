import BirthdayService from '../../../src/command/birthday/birthday.service';
import EnvVarService from '../../../src/service/envvar.service';
import QueryService from '../../../src/command/birthday/query.service';
import { ChatInputCommandInteraction, ModalSubmitInteraction } from 'discord.js';
import { QueryResult } from 'pg';
import DatetimeService from '../../../src/service/datetime.service';
import { DateTime } from 'luxon';
import dedent from 'ts-dedent';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import Birthday from '../../../src/model/birthday';
import { LibSQLDatabase } from 'drizzle-orm/libsql';

vi.mock('drizzle-orm/libsql');
vi.mock('../../../src/service/envvar.service');
vi.mock('../../../src/command/birthday/query.service');
vi.mock('../../../src/service/datetime.service');

describe('BirthdayService', () => {
    let envVarService: EnvVarService;
    let queryService: QueryService;
    let service: BirthdayService;

    beforeEach(() => {
        envVarService = new EnvVarService();
        queryService = new QueryService({} as unknown as LibSQLDatabase);
        service = new BirthdayService(envVarService, queryService);
    });

    describe('list', () => {
        it('should not work in a direct message', async () => {
            const interaction = {
                inGuild: vi.fn().mockReturnValue(false),
            } as unknown as ChatInputCommandInteraction;

            const actual = await service.list(interaction);

            expect(actual).toEqual('This command must be used in a server.');
        });

        it('should return a list of birthdays', async () => {
            DatetimeService.now = vi.fn().mockReturnValue(DateTime.fromObject({
                day: 1,
                month: 2,
                year: 2023,
            }));

            const interaction = {
                inGuild: vi.fn().mockReturnValue(true),
                guildId: '1234',
            } as unknown as ChatInputCommandInteraction;

            const birthdays: Birthday[] = [
                  {
                      discord_id: '1234',
                      day: 1,
                      month: 1,
                      year: 2000,
                      timezone: 'America/New_York',
                  },
                  {
                      discord_id: '1236',
                      day: 1,
                      month: 3,
                      year: 2000,
                      timezone: 'America/New_York',
                  },
                  {
                      discord_id: '1235',
                      day: 1,
                      month: 5,
                      year: null,
                      timezone: 'America/New_York',
                  },
              ];

            queryService.getBirthdays.mockReturnValue(Promise.resolve(birthdays));

            const actual: string = await service.list(interaction);

            const expected: string = dedent`Birthdays in Date Order:
                <@1236> - 01 March - 28 days away - 23 years old
                <@1235> - 01 May - 89 days away
                ----- 2024
                <@1234> - 01 January - 334 days away - 24 years old
                Average age is 22.5`;

            expect(actual).toEqual(expected);
        });

        it('should return a message if there are no birthdays', async () => {
            const interaction = {
                inGuild: vi.fn().mockReturnValue(true),
                guildId: '1234',
            } as unknown as ChatInputCommandInteraction;

            queryService.getBirthdays.mockReturnValue(Promise.resolve([]));

            const actual: string = await service.list(interaction);

            const expected: string = 'No birthdays set.';

            expect(actual).toEqual(expected);
        });
    });

    describe('remove', () => {
        it('should not work in a direct message', async () => {
            const interaction = {
              inGuild: vi.fn().mockReturnValue(false),
            } as unknown as ChatInputCommandInteraction;

            const actual = await service.remove(interaction);

            expect(actual).toEqual('This command must be used in a server.');
        });

        it('should report birthday removed', async () => {
            const interaction = {
              inGuild: vi.fn().mockReturnValue(true),
              guildId: '1234',
              user: {
                id: '12345',
              },
            } as unknown as ChatInputCommandInteraction;

            const birthday: QueryResult = {
                command: '',
                fields: [],
                oid: 0,
                rowCount: 1,
                rows: [{
                    discord_id: '1234',
                    day: 1,
                    month: 1,
                    year: 2000,
                    timezone: 'America/New_York',
                }],
            };

            queryService.getBirthday.mockReturnValue(Promise.resolve(birthday));

            const actual: string = await service.remove(interaction);

            expect(actual).toEqual('Removed birthday for <@12345>.');
        });

        it('should report no birthday set', async () => {
            const interaction = {
              inGuild: vi.fn().mockReturnValue(true),
              guildId: '1234',
              user: {
                id: '12345',
              },
            } as unknown as ChatInputCommandInteraction;

            queryService.getBirthday.mockReturnValue(Promise.resolve(undefined));

            const actual = await service.remove(interaction);

            expect(actual).toEqual('No birthday set for <@12345>.');
        });
    });

    describe('set', () => {
        it('should not work in a direct message', async () => {

          const interaction = {
            inGuild: vi.fn().mockReturnValue(false),
          } as unknown as ModalSubmitInteraction;

            const actual = await service.set(interaction);

            expect(actual).toEqual('This command must be used in a server.');
        });

        it('should report invalid date', async () => {
          const interaction = {
            inGuild: vi.fn().mockReturnValue(true),
            guildId: '1234',
            user: {
              id: '12345',
            },
            fields: {
              getTextInputValue: vi.fn().mockReturnValue('aa'),
            },
          } as unknown as ModalSubmitInteraction;

            const actual = await service.set(interaction);

            expect(actual).toEqual('Invalid date.');
        });

        it('should report birthday set', async () => {

          const interaction = {
            inGuild: vi.fn().mockReturnValue(true),
            guildId: '1234',
            user: {
              id: '12345',
            },
            fields: {
              getTextInputValue: vi.fn(),
            },
          } as unknown as ModalSubmitInteraction;

            interaction.fields.getTextInputValue.mockReturnValueOnce('1');
            interaction.fields.getTextInputValue.mockReturnValueOnce('1');
            interaction.fields.getTextInputValue.mockReturnValueOnce('2000');
            interaction.fields.getTextInputValue.mockReturnValueOnce('America/New_York');

            const birthday: QueryResult = {
                command: '',
                fields: [],
                oid: 0,
                rowCount: 0,
                rows: [],
            };

            queryService.getBirthday.mockReturnValue(Promise.resolve(birthday));

            const actual = await service.set(interaction);

            expect(actual).toEqual('Setting <@12345>\'s birthday to 1 January 2000.');
        });
    });
});
