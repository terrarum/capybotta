import 'dotenv/config';
import * as process from 'process';

export interface EnvironmentVariables {
  [key: string]: string;

  DISCORD_TOKEN: string;
  DISCORD_CLIENT_ID: string;
  DATABASE_URL: string;
  DEFAULT_TIMEZONE: string;
  ANNOUNCEMENT_CHANNEL_ID: string;
  BIRTHDAY_CHANNEL_ID: string;
  GENERAL_CHANNEL_ID: string;
  SENTRY_DSN: string;
  VIC_API_KEY: string;
  OWM_KEY: string;
}

export default class EnvVarService {
  private environmentVariables: EnvironmentVariables | undefined;

  private variables: string[] = [
    'DISCORD_TOKEN',
    'DISCORD_CLIENT_ID',
    'TURSO_CONNECTION_URL',
    'TURSO_AUTH_TOKEN',
    'ANNOUNCEMENT_CHANNEL_ID',
    'BIRTHDAY_CHANNEL_ID',
    'GENERAL_CHANNEL_ID',
    'SENTRY_DSN',
    'VIC_API_KEY',
    'OWM_KEY',
  ];

  constructor() {
    this.load();
  }

  private getVariable(variable: string): string {
    if (!this.environmentVariables) {
      throw new Error('Environment variables must be loaded.');
    }
    const value = this.environmentVariables[variable];
    if (!value) {
      throw new Error(`${variable} must be set.`);
    }
    return value;
  }

  public getDiscordToken(): string {
    return this.getVariable('DISCORD_TOKEN');
  }

  public getDiscordClientId(): string {
    return this.getVariable('DISCORD_CLIENT_ID');
  }

  public getTursoConnectionUrl(): string {
    return this.getVariable('TURSO_CONNECTION_URL');
  }

  public getTursoAuthToken(): string {
    return this.getVariable('TURSO_AUTH_TOKEN');
  }

  public getDefaultTimezone(): string {
    return 'Australia/Melbourne';
  }

  public getAnnouncementChannelId(): string {
    return this.getVariable('ANNOUNCEMENT_CHANNEL_ID');
  }

  public getBirthdayChannelId(): string {
    return this.getVariable('BIRTHDAY_CHANNEL_ID');
  }

  public getGeneralChannelId(): string {
    return this.getVariable('GENERAL_CHANNEL_ID');
  }

  public getSentryDsn(): string {
    return this.getVariable('SENTRY_DSN');
  }

  public getVicApiKey(): string {
    return this.getVariable('VIC_API_KEY');
  }

  public getOwmKey(): string {
    return this.getVariable('OWM_KEY');
  }

  private load(): void {
    this.environmentVariables = this.variables.reduce((envVars, variable) => {
      const value = process.env[variable];
      if (!value) {
        throw new Error(`${variable} must be set.`);
      }
      return {
        ...envVars,
        [variable]: value,
      };
    }, {} as EnvironmentVariables);
  }
}
