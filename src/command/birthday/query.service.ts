import Birthday from '../../model/birthday';
import { LibSQLDatabase } from 'drizzle-orm/libsql';
import { birthdayTable } from '../../schema/birthday';
import { and, eq } from 'drizzle-orm';

export default class QueryService {
  private databaseClient: LibSQLDatabase;

  constructor(databaseClient: LibSQLDatabase) {
    this.databaseClient = databaseClient;
  }

  public async getBirthday(userId: string, serverId: string): Promise<Birthday | undefined> {
    const result: Birthday[] = await this.databaseClient
      .select()
      .from(birthdayTable)
      .where(
        and(
          eq(birthdayTable.discord_id, userId),
          eq(birthdayTable.server_id, serverId)
        )
      )
      .limit(1);

    return result[0];
  }

  public async setBirthday(
    userId: string,
    serverId: string,
    day: number,
    month: number,
    year: number | null,
    timezone: string,
  ): Promise<void> {
    console.log('setBirthday', userId, serverId, day, month, year, timezone);
    const birthday: Birthday | undefined = await this.getBirthday(userId, serverId);

    // TODO upsert
    if (!birthday) {
      console.log('inserting');
      try {
        await this.databaseClient.insert(birthdayTable)
          .values({
            discord_id: userId,
            server_id: serverId,
            day,
            month,
            year,
            timezone,
          });
      } catch (e) {
        console.error('Error setting birthday', e);
      }
    }
    else {
      console.log('updating');
      try {
        await this.databaseClient.update(birthdayTable)
          .set({
            day,
            month,
            year,
            timezone,
          })
          .where(
            and(
              eq(birthdayTable.discord_id, userId),
              eq(birthdayTable.server_id, serverId)
            )
          );
      }
      catch (e) {
        console.error('Error updating birthday', e);
      }
    }
  }

  public async removeBirthday(userId: string, serverId: string): Promise<void> {
    await this.databaseClient
      .delete(birthdayTable)
      .where(
        and(
          eq(birthdayTable.discord_id, userId),
          eq(birthdayTable.server_id, serverId)
        )
      );
  }

  public async getBirthdays(serverId: string): Promise<Birthday[]> {
    return this.databaseClient.select()
      .from(birthdayTable)
      .where(eq(birthdayTable.server_id, serverId));
  }

  // get birthdays of server by day and month
  public async getBirthdaysByDayAndMonth(day: number, month: number): Promise<Birthday[]> {
    return this.databaseClient.select()
      .from(birthdayTable)
      .where(
        and(
          eq(birthdayTable.day, day),
          eq(birthdayTable.month, month)
        )
      );
  }
}
