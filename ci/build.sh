#!/bin/bash

# Build and Push base image to GitLab Docker Registry.
# Run this script from the root of this project.
docker build -t registry.gitlab.com/terrarum/capybotta/ci-base-image ./ci/
docker push registry.gitlab.com/terrarum/capybotta/ci-base-image
