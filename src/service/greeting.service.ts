import { DateTime } from 'luxon';
import RandomService from '../utils/random.service';

export default class GreetingService {
  private greetings: string[][] = [
    [
      'Good morning. Despite our best efforts, it is once again Monday',
      "It's Monday! Woo!",
      "Monday returns, and so do we",
    ],
    [
      "It's Tuesday! Mixed feelings but it's something",
      "Tuesday! The Friday of Wednesday",
      "Oi m8 it's chewsday innit",
    ],
    [
      "Who doesn't love a good Wednesday?",
      "It's Wednesday! Half way there!",
      "Wednesday is here! You are reading words!"
    ],
    [
      "THURSDAY. If this was a long weekend, it'd be Friday!",
      "The Thur in Thursday stands for Thursday",
      "It's Friday tomorrow! Today is Thursday",
    ],
    [
      "Awww shit it's Friday let's gooooo",
      "I can't feel my teeth",
      "Friday! Got any plans?"
    ],
    [
      "It's Saturday! Full of potential!",
      "Weekend babyyyy it's Saturday",
      "Saturday! Get brunch, you deserve it!",
    ],
    [
      "It's Sunday! Have you done laundry?",
      "You should sleep in, it's Sunday.",
      "Sunday has unda in it because we live down unda",
    ],
  ]

  public getGreeting(): string {
    const now: DateTime = DateTime.now().setZone('Australia/Melbourne');
    const weekday: number = now.weekday

    const greetings: string[] | undefined = this.greetings[weekday - 1];

    if (!greetings) {
      return 'Good morning!';
    }

    const greeting: string | undefined = greetings[RandomService.randRange(0, greetings.length - 1)];

    return greeting ?? 'Good morning!';
  }
}
