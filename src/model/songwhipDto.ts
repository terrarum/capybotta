export interface MusicServiceLink {
  link: string;
  countries: string[] | null;
}

export interface Artist {
  name: string;
}

export interface SongItem {
  type: string;
  name: string;
  url: string;
  image: string;
  linksCountries: string[];
  links: { [key: string]: MusicServiceLink[] };
  artists: Artist[];
}

export interface SongData {
  item: SongItem;
}

export interface SongwhipDto {
  status: string;
  data: SongData;
}
