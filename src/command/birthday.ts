import * as Sentry from '@sentry/node';
import {
  ActionRowBuilder,
  ChatInputCommandInteraction,
  ModalActionRowComponentBuilder,
  ModalBuilder,
  SlashCommandBuilder,
  SlashCommandSubcommandsOnlyBuilder,
  TextInputBuilder,
  TextInputStyle
} from 'discord.js';
import Command from '../interface/command';
import BirthdayService from './birthday/birthday.service';

export default class Birthday implements Command {
  private birthdayService: BirthdayService;

  constructor(birthdayService: BirthdayService) {
    this.birthdayService = birthdayService;
  }

  public data: SlashCommandSubcommandsOnlyBuilder = new SlashCommandBuilder()
    .setName('birthday')
    .setDescription('Set and list user birthdays')
    .addSubcommand((subcommand) => subcommand.setName('list').setDescription('List user birthdays'))
    .addSubcommand((subcommand) => subcommand.setName('set').setDescription('Opens a form to set your birthday'))
    .addSubcommand((subcommand) => subcommand.setName('remove').setDescription('Remove your birthday'));

  async execute(interaction: ChatInputCommandInteraction): Promise<void> {
    const subcommand = interaction.options.getSubcommand();
    if (subcommand === 'list') {
      await interaction.deferReply();
      const birthdayList: string = await this.birthdayService.list(interaction);
      try {
        await interaction.editReply(birthdayList);
      } catch (e) {
        Sentry.captureException(e);
        console.error(e);
      }
    } else if (subcommand === 'remove') {
      await interaction.deferReply();
      const removeMessage: string = await this.birthdayService.remove(interaction);
      try {
        await interaction.editReply(removeMessage);
      } catch (e) {
        Sentry.captureException(e);
        console.error(e);
      }
    } else if (subcommand === 'set') {
      const modal: ModalBuilder = new ModalBuilder().setCustomId('birthdaySetModal').setTitle('Set your birthday.');

      const dayInput: TextInputBuilder = new TextInputBuilder()
        .setStyle(TextInputStyle.Short)
        .setCustomId('birthdaySetDay')
        .setLabel('Day')
        .setMinLength(1)
        .setMaxLength(2)
        .setPlaceholder('27')
        .setRequired(true);
      const monthInput: TextInputBuilder = new TextInputBuilder()
        .setStyle(TextInputStyle.Short)
        .setCustomId('birthdaySetMonth')
        .setLabel('Month')
        .setMinLength(1)
        .setMaxLength(2)
        .setPlaceholder('10')
        .setRequired(true);
      const yearInput: TextInputBuilder = new TextInputBuilder()
        .setStyle(TextInputStyle.Short)
        .setCustomId('birthdaySetYear')
        .setLabel('Year (optional)')
        .setMinLength(4)
        .setMaxLength(4)
        .setPlaceholder('')
        .setRequired(false);
      const timezoneInput: TextInputBuilder = new TextInputBuilder()
        .setStyle(TextInputStyle.Short)
        .setCustomId('birthdaySetTimezone')
        .setLabel('IANA Time Zone (optional)')
        .setPlaceholder('Australia/Melbourne')
        .setRequired(false);

      modal.addComponents(
        new ActionRowBuilder<ModalActionRowComponentBuilder>().addComponents(dayInput),
        new ActionRowBuilder<ModalActionRowComponentBuilder>().addComponents(monthInput),
        new ActionRowBuilder<ModalActionRowComponentBuilder>().addComponents(yearInput),
        new ActionRowBuilder<ModalActionRowComponentBuilder>().addComponents(timezoneInput),
      );

      try {
        await interaction.showModal(modal);
      } catch (e) {
        Sentry.captureException(e);
        console.error(e);
      }
    }
  }
}
