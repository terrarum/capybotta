import StringService from '../../src/utils/string.service.js';
import { describe, expect, it } from 'vitest';

describe('StringService', () => {
    it.each([
        ['test', 'Test'],
        ['testString', 'Test String'],
        ['testStringTest', 'Test String Test'],
        ['test string', 'Test string'],
    ])('should convert %s to %s', (input: string, expected: string) => {
        expect(StringService.camelToTitleCase(input)).toEqual(expected);
    });
});
