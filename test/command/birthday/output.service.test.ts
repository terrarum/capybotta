import OutputService from '../../../src/command/birthday/output.service';
import { DateTime } from 'luxon';
import dedent from 'ts-dedent';
import { describe, expect, it } from 'vitest';

describe('BirthdayOutputService', () => {
  it('should return a sorted list of birthdays', () => {
    const now = DateTime.fromObject(
      {day: 10, month: 6, year: 2023, hour: 11, minute: 11, second: 11, millisecond: 111},
    );

    const actual = OutputService.list(now, [
      {discord_id: '2', day: 9, month: 6, year: null, timezone: 'Australia/Melbourne'},
      {discord_id: '1', day: 1, month: 5, year: 1990, timezone: 'Australia/Melbourne'},
      {discord_id: '4', day: 3, month: 7, year: 1980, timezone: 'Australia/Melbourne'},
      {discord_id: '3', day: 11, month: 6, year: null, timezone: 'Australia/Melbourne'},
      {discord_id: '5', day: 10, month: 6, year: null, timezone: 'Australia/Melbourne'},
    ]);

    const expected: string = dedent`Birthdays in Date Order:
    <@5> - 10 June - Today!
    <@3> - 11 June - 1 day away!
    <@4> - 03 July - 23 days away - 43 years old
    ----- 2024
    <@1> - 01 May - 325 days away - 34 years old
    <@2> - 09 June - 364 days away
    Average age is 37.5`;

    expect(actual).toEqual(expected);
  });

  it('should return a sorted list of birthdays with no average if years are not set', () => {
    const now = DateTime.fromObject(
      {day: 10, month: 6, year: 2023, hour: 11, minute: 11, second: 11, millisecond: 111},
    );

    const actual = OutputService.list(now, [
      {discord_id: '2', day: 9, month: 6, year: null, timezone: 'Australia/Melbourne'},
      {discord_id: '1', day: 1, month: 5, year: null, timezone: 'Australia/Melbourne'},
      {discord_id: '4', day: 3, month: 7, year: null, timezone: 'Australia/Melbourne'},
      {discord_id: '3', day: 11, month: 6, year: null, timezone: 'Australia/Melbourne'},
      {discord_id: '5', day: 10, month: 6, year: null, timezone: 'Australia/Melbourne'},
    ]);

    const expected: string = dedent`Birthdays in Date Order:
    <@5> - 10 June - Today!
    <@3> - 11 June - 1 day away!
    <@4> - 03 July - 23 days away
    ----- 2024
    <@1> - 01 May - 325 days away
    <@2> - 09 June - 364 days away`;

    expect(actual).toEqual(expected);
  });
});
