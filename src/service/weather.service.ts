import EnvVarService from './envvar.service';
import WeatherReport from '../model/owm/WeatherReport';
import PollutionReport from '../model/owm/PollutionReport';

export default class WeatherService {
  private envVarService: EnvVarService;
  private weatherUrl: string = 'https://api.openweathermap.org/data/3.0/onecall';
  private pollutionUrl: string = 'http://api.openweathermap.org/data/2.5/air_pollution';

  private melbourneCoords = {
    lat: '-37.812229',
    lon: '144.962261',
  }

  constructor(envVarService: EnvVarService) {
    this.envVarService = envVarService;
  }

  public async getWeatherReport(): Promise<WeatherReport> {
    const params: URLSearchParams = new URLSearchParams();
    params.set('appid', this.envVarService.getOwmKey());
    params.set('exclude', 'minutely');
    params.set('units', 'metric');
    params.set('lat', this.melbourneCoords.lat);
    params.set('lon', this.melbourneCoords.lon);

    const url = `${this.weatherUrl}?${params.toString()}`;

    const response = await (await fetch(url)).json();

    return response === '[]' ? null : response;
  }

  public async getAirPollution(): Promise<PollutionReport> {
    const params: URLSearchParams = new URLSearchParams();
    params.set('appid', this.envVarService.getOwmKey());
    params.set('lat', this.melbourneCoords.lat);
    params.set('lon', this.melbourneCoords.lon);

    const url = `${this.pollutionUrl}?${params.toString()}`;

    const response = await (await fetch(url)).json();

    return response === '[]' ? null : response;
  }
}
