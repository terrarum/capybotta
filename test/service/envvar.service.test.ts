import EnvVarService from '../../src/service/envvar.service.js';
import { afterEach, describe, expect, it, vi } from 'vitest';

describe('EnvVarService', () => {
    afterEach(() => {
      vi.unstubAllEnvs();
    });

    it('should load environment variables', () => {
        vi.stubEnv('DISCORD_CLIENT_ID', 'discord_client_id');
        vi.stubEnv('DISCORD_TOKEN', 'discord_token');
        vi.stubEnv('TURSO_CONNECTION_URL', 'database_url');
        vi.stubEnv('TURSO_AUTH_TOKEN', 'turso_auth_token');
        vi.stubEnv('ANNOUNCEMENT_CHANNEL_ID', 'announcement_channel_id');
        vi.stubEnv('BIRTHDAY_CHANNEL_ID', 'birthday_channel_id');
        vi.stubEnv('GENERAL_CHANNEL_ID', 'general_channel_id');
        vi.stubEnv('SENTRY_DSN', 'sentry_dsn');
        vi.stubEnv('VIC_API_KEY', 'vic_api_key');
        vi.stubEnv('OWM_KEY', 'owm_key');

        const envVarService = new EnvVarService();

        expect(envVarService.getDiscordClientId()).toEqual('discord_client_id');
        expect(envVarService.getDiscordToken()).toEqual('discord_token');
        expect(envVarService.getTursoConnectionUrl()).toEqual('database_url');
        expect(envVarService.getTursoAuthToken()).toEqual('turso_auth_token');
        expect(envVarService.getAnnouncementChannelId()).toEqual('announcement_channel_id')
        expect(envVarService.getBirthdayChannelId()).toEqual('birthday_channel_id');
        expect(envVarService.getGeneralChannelId()).toEqual('general_channel_id');
        expect(envVarService.getSentryDsn()).toEqual('sentry_dsn');
        expect(envVarService.getVicApiKey()).toEqual('vic_api_key');
        expect(envVarService.getOwmKey()).toEqual('owm_key');
    });

    it('should throw an error if an env var is missing', () => {
        delete process.env.DISCORD_TOKEN;
        expect(() => new EnvVarService()).toThrowError('DISCORD_TOKEN must be set.');
    });
});
