import { vi, describe, it, expect, beforeEach } from 'vitest';
import EnvVarService from '../../src/service/envvar.service';
import { Client, TextChannel } from 'discord.js';
import VicDatesService from '../../src/cron/vic-dates.service';
import { DateTime } from 'luxon';

vi.mock('../../src/service/envvar.service');
vi.mock("discord.js");

const mockFetch = vi.fn()
global.fetch = mockFetch

describe("VicDatesService", () => {
  let envVarService: EnvVarService;
  let discordClient: Client;
  let vicDatesService: VicDatesService;

  const validDateTime: DateTime = DateTime.fromObject({
    year: 2020,
    month: 1,
    day: 1,
    hour: 6,
    minute: 0,
    second: 0,
  });

  const invalidDateTime: DateTime = DateTime.fromObject({
    year: 2020,
    month: 13,
    day: 1,
    hour: 6,
    minute: 0,
    second: 0,
  });

  beforeEach(() => {
    mockFetch.mockReset()
    envVarService = new EnvVarService();
    discordClient = {
      channels: {
        fetch: vi.fn(),
      },
    } as unknown as Client;
    vicDatesService = new VicDatesService(envVarService, discordClient);
  });

  // It should throw an error if the dates are invalid.
  it('should throw an error if the dates are invalid', async () => {
    expect(async () => {
      await vicDatesService.update(invalidDateTime)
    }).rejects.toThrowError('Invalid date');

    expect(mockFetch).not.toHaveBeenCalled();
  });

  // It should throw an error if the response is not ok.
  it('should throw an error if the response is not ok', async () => {
    mockFetch.mockResolvedValueOnce({
      ok: false,
      statusText: 'Bad request'
    });

    expect(async () => {
      await vicDatesService.update(validDateTime)
    }).rejects.toThrowError('Bad request');

    expect(mockFetch).toHaveBeenCalled()
  });

  // It should remind people about events if they are within the set number of days.
  it('should remind people about events if they are within the set number of days', async () => {
    mockFetch.mockResolvedValueOnce({
      ok: true,
      json: vi.fn(() => ({
        dates: [
          {
            date: '2020-01-01',
            name: 'Today',
            type: 'PUBLIC_HOLIDAY',
          },
          {
            date: '2020-01-02',
            name: 'One Day Away',
            type: 'PUBLIC_HOLIDAY',
          },
          {
            date: '2020-01-08',
            name: 'Seven Days Away',
            type: 'DAYLIGHT_SAVING',
          },
          {
            date: '2020-01-31',
            name: 'Thirty Days Away',
            type: 'PUBLIC_HOLIDAY',
          },
        ]
      }))
    });

    const mockChannel = {
      send: vi.fn(),
    } as unknown as TextChannel;
    discordClient.channels.fetch.mockReturnValue(Promise.resolve(mockChannel));

    await vicDatesService.update(validDateTime);

    const expected: string = `One Day Away is in 1 day.\nSeven Days Away in 7 days.\nThirty Days Away is in 30 days.`;

    expect(mockFetch).toHaveBeenCalled();
    expect(discordClient.channels.fetch).toHaveBeenCalled();
    expect(mockChannel.send).toHaveBeenCalledWith(expected);
  });

  // It should not remind people about events if they are not within the set number of days.
  it('should not remind people about events if they are not within the set number of days', async () => {
    mockFetch.mockResolvedValueOnce({
      ok: true,
      json: vi.fn(() => ({
        dates: [
          {
            date: '2020-01-01',
            name: 'Today',
            type: 'PUBLIC_HOLIDAY',
          },
          {
            date: '2020-01-03',
            name: 'One Day Away',
            type: 'PUBLIC_HOLIDAY',
          },
          {
            date: '2020-01-09',
            name: 'Seven Days Away',
            type: 'DAYLIGHT_SAVING',
          },
          {
            date: '2020-01-30',
            name: 'Thirty Days Away',
            type: 'PUBLIC_HOLIDAY',
          },
        ]
      }))
    });

    const mockChannel = {
      send: vi.fn(),
    } as unknown as TextChannel;
    discordClient.channels.fetch.mockReturnValue(Promise.resolve(mockChannel));

    await vicDatesService.update(validDateTime);

    expect(mockFetch).toHaveBeenCalled();
    expect(discordClient.channels.fetch).not.toHaveBeenCalled();
    expect(mockChannel.send).not.toHaveBeenCalled();
  });
});
