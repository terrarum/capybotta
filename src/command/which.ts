import * as Sentry from '@sentry/node';
import * as console from 'console';
import { ChatInputCommandInteraction, SlashCommandBuilder } from 'discord.js';
import Command from '../interface/command.js';
import RandomService from '../utils/random.service.js';

export default class Which implements Command {
  public data: Omit<SlashCommandBuilder, 'addSubcommand' | 'addSubcommandGroup'> = new SlashCommandBuilder()
    .setName('which')
    .setDescription('Picks an option at random')
    .addStringOption((option) => option.setName('options').setDescription('Your options').setRequired(true));

  async execute(interaction: ChatInputCommandInteraction) {
    let text: string = '';
    try {
      text = interaction.options.getString('options', true);
    } catch (e) {
      Sentry.captureException(e);
      console.error('Error getting option "options"', e);
    }

    const chosenOption: string = this.getOption(text);

    await interaction.reply(`From \`${text}\`\nI choose \`${chosenOption}\``);
  }

  private getRandomOption(options: string[]): string {
    const index = RandomService.getRandomInt(options.length);
    const response = options[index];
    if (response === undefined) {
      throw new Error('Response is undefined');
    }

    return response;
  }

  private getOptions(text: string): string[] {
    const options: string[] = text.includes(',') ? text.split(',') : text.split(' ');

    return options.filter((option) => option !== '').map((option) => option.trim());
  }

  private getOption(text: string): string {
    const options = this.getOptions(text);
    return this.getRandomOption(options);
  }
}
