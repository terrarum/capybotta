export default interface Birthday {
  discord_id: string;
  day: number;
  month: number;
  year: number | null;
  timezone: string;
}
