import ValidationService from '../../../src/command/birthday/validation.service';
import { describe, expect, it } from 'vitest';

describe('BirthdayValidationService', () => {
  it.each([
    ['20', '10', '1990', 'Australia/Melbourne', true],
    ['09', '9', '1990', 'Australia/Melbourne', true],
    ['9', '09', '1990', 'Australia/Melbourne', true],
    ['9', '9', '', 'Australia/Melbourne', true],
    ['aa', '12', '1990', 'Australia/Melbourne', false],
    ['30', 'aa', '1990', 'Australia/Melbourne', false],
    ['30', '12', 'aaaa', 'Australia/Melbourne', false],
    ['30', '12', '1990', 'Atlantis/Atlantis', false],
    ['32', '12', '1990', 'Australia/Melbourne', false],
    ['30', '13', '1990', 'Australia/Melbourne', false],
    ['30', '12', '2990', 'Australia/Melbourne', false],
    ['29', '02', '2015', 'Australia/Melbourne', false],
    ['29', '02', '2016', 'Australia/Melbourne', true],
    ['30', '02', '2016', 'Australia/Melbourne', false],
  ])(`should validate %s/%s/%s %s as %s`, (day: string, month: string, year: string, timezone: string, expected: boolean) => {
    expect(ValidationService.validate(day, month, year, timezone)).toEqual(expected);
  });
});
