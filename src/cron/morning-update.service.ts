import { Client, TextChannel } from 'discord.js';
import { DateTime } from 'luxon';
import EnvVarService from '../service/envvar.service';
import WeatherService from '../service/weather.service';
import WeatherReport from '../model/owm/WeatherReport';
import PollutionReport from '../model/owm/PollutionReport';
import Daily from '../model/owm/Daily';
import Pollution from '../model/owm/Pollution';
import Alert from '../model/owm/Alert';
import GreetingService from '../service/greeting.service';
import { getPollenLevel } from './utils/pollen';
import dedent from 'ts-dedent';

export default class VicDatesService {
  envVarService: EnvVarService;
  discordClient: Client;
  weatherService: WeatherService;
  greetingService: GreetingService;

  constructor(envVarService: EnvVarService, discordClient: Client, weatherService: WeatherService, greetingService: GreetingService) {
    this.envVarService = envVarService;
    this.discordClient = discordClient;
    this.weatherService = weatherService;
    this.greetingService = greetingService;
  }

  private async sendMessage(message: string): Promise<void> {
    const channel: TextChannel = (await this.discordClient.channels.fetch(
      this.envVarService.getGeneralChannelId(),
    )) as TextChannel;
    await channel.send(message);
  }

  private uvi(uvi: number): string {
    let severity: string = '';
    if (uvi < 3) severity = 'Low';
    if (uvi >= 3 && uvi < 6) severity = 'Moderate';
    if (uvi >= 6 && uvi < 8) severity = 'High';
    if (uvi >= 8 && uvi < 11) severity = 'Very High';
    if (uvi > 11) severity = 'Extreme';

    return severity;
  }

  private sunscreen(uvi: number): string {
    if (uvi < 3) return 'No need for sunscreen today';
    if (uvi >= 3 && uvi < 6) return "Wear sunscreen if you're going to be in the sun all day";
    if (uvi >= 6 && uvi < 11) return 'Sunscreen highly recommended';
    if (uvi >= 11) return 'Wear sunscreen indoors';
    return '';
  }

  private airQuality(airQualityIndex: number): string {
    if (airQualityIndex === 1) return 'good';
    if (airQualityIndex === 2) return 'fair';
    if (airQualityIndex === 3) return 'moderate';
    if (airQualityIndex === 4) return 'poor';
    if (airQualityIndex === 5) return 'very poor';
    return '';
  }

  private rainIntensity(rain: number): string {
    if (rain < 10) return 'light';
    if (rain >= 10 && rain < 50) return 'moderate';
    if (rain >= 50) return 'heavy';
    return '';
  }

  private dateString(timestamp: number): string {
    return DateTime.fromSeconds(timestamp)
      .setZone('Australia/Melbourne')
      .setLocale('en-au')
      .toLocaleString(DateTime.DATE_FULL) ?? timestamp.toString()
  }

  private moonStatus(moonPhase: number): string {
    if (moonPhase === 0) {
      return ':new_moon: new';
    }
    else if (moonPhase > 0 && moonPhase < 0.25) {
      return ':waxing_crescent_moon: waxing crescent';
    }
    else if (moonPhase === 0.25) {
      return ':first_quarter_moon: first-quarter';
    }
    else if (moonPhase > 0.25 && moonPhase < 0.5) {
      return ':waxing_gibbous_moon: waxing gibbous';
    }
    else if (moonPhase === 0.5) {
      return ':full_moon: full';
    }
    else if (moonPhase > 0.5 && moonPhase < 0.75) {
      return ':waning_gibbous_moon: waning gibbous';
    }
    else if (moonPhase === 0.75) {
      return ':last_quarter_moon: last quarter';
    }
    else if (moonPhase > 0.75 && moonPhase < 1) {
      return ':waning_crescent_moon: waning crescent';
    }
    else if (moonPhase === 1) {
      return ':new_moon: new';
    }

    return '';
  }

  async update(): Promise<void> {
    const weatherReport: WeatherReport = await this.weatherService.getWeatherReport();
    const pollutionReport: PollutionReport = await this.weatherService.getAirPollution();

    const dailyWeather: Daily | undefined = weatherReport.daily[0] ;
    const dailyPollution: Pollution | undefined = pollutionReport.list[0];

    if (!weatherReport || !pollutionReport || !dailyWeather || !dailyPollution) {
      await this.sendMessage('Could not retrieve weather data :(');
      throw new Error('Could not retrieve weather or pollution data');
    }

    const date: string = this.dateString(dailyWeather.dt);

    const uvIndex: number = dailyWeather.uvi;
    const airQualityIndex: number = dailyPollution.main.aqi;
    const moonPhase: number = dailyWeather.moon_phase;
    const summary: string = dailyWeather.summary;
    const chanceOfRain: number = dailyWeather.pop * 100;
    const amountOfRain: number | undefined = dailyWeather?.rain;
    const humidity: number = dailyWeather.humidity;
    const minTemp: number = Math.round(dailyWeather.temp.min);
    const maxTemp: number = Math.round(dailyWeather.temp.max);
    const feelsLikeMorning: string = `${Math.round(dailyWeather.feels_like.morn)}°C`.padStart(4, ' ');
    const feelsLikeDay: string = `${Math.round(dailyWeather.feels_like.day)}°C`.padStart(4, ' ');
    const feelsLikeEvening: string = `${Math.round(dailyWeather.feels_like.eve)}°C`.padStart(4, ' ');
    const feelsLikeNight: string = `${Math.round(dailyWeather.feels_like.night)}°C`.padStart(4, ' ');

    const pollenLevel: string = await getPollenLevel();
    const rainIntensity: string = amountOfRain ? this.rainIntensity(amountOfRain) : '';

    const message: string = dedent`# ${this.greetingService.getGreeting()}
      It's the Capy Weather Report for ${date}! ${summary}.

      Today's temperature is going to range from **${minTemp}°C** to **${maxTemp}°C**, but here are the feels likes:
      \`\`\`
      6am  12am 6pm  12pm
      ${feelsLikeMorning} ${feelsLikeDay} ${feelsLikeEvening} ${feelsLikeNight}
      \`\`\`
      Today's UV Index has a rating of **${this.uvi(uvIndex)}** at **${uvIndex}**. ${this.sunscreen(uvIndex)}.

      There is a **${chanceOfRain}%** chance of ${rainIntensity + ' '}rain. Humidity is at **${humidity}%**, air quality is **${this.airQuality(airQualityIndex)}**, pollen levels are **${pollenLevel}** and the moon is **${this.moonStatus(moonPhase)}**.
    `;

    await this.sendMessage(message);
  }
}
