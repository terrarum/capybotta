import { sql } from 'drizzle-orm';
import { integer, sqliteTable, text, uniqueIndex } from 'drizzle-orm/sqlite-core';

export const birthdayTable = sqliteTable('birthday', {
  id: integer().primaryKey({ autoIncrement: true }),
  discord_id: text().notNull(),
  server_id: text().notNull(),
  day: integer().notNull(),
  month: integer().notNull(),
  year: integer(),
  timezone: text().notNull(),
  updated_on: integer({ mode: 'timestamp' }).default(sql`CURRENT_TIMESTAMP`),
}, (table) => {
  return {
    birthdayIdx: uniqueIndex("birthday_server_user_index").on(table.discord_id, table.server_id),
  };
});
