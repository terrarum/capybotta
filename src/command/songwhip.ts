import * as Sentry from '@sentry/node';
import console from 'console';
import {
  ActionRowBuilder,
  ButtonBuilder,
  ButtonStyle,
  ChatInputCommandInteraction,
  SlashCommandBuilder
} from 'discord.js';
import Command from '../interface/command.js';
import SongLink from '../model/songlink.js';
import { SongwhipDto } from '../model/songwhipDto.js';
import SongwhipService from './songwhip/songwhip.service.js';

export default class Songwhip implements Command {
  private songwhipService: SongwhipService;

  public data: Omit<SlashCommandBuilder, 'addSubcommand' | 'addSubcommandGroup'> = new SlashCommandBuilder()
    .setName('songwhip')
    .setDescription('Returns platform links for the given song')
    .addStringOption((option) => option.setName('song').setDescription('URL of the song').setRequired(true));

  constructor(songwhipService: SongwhipService) {
    this.songwhipService = songwhipService;
  }

  async execute(interaction: ChatInputCommandInteraction): Promise<void> {
    await interaction.deferReply();

    let searchTerm: string = '';
    try {
      searchTerm = interaction.options.getString('song', true);
    } catch (e) {
      Sentry.captureException(e);
      console.error('Error getting option "song"', e);
    }

    let response: SongwhipDto;

    try {
      response = await this.songwhipService.getSong(searchTerm);
    } catch (e) {
      Sentry.captureException(e);
      console.error('Error getting song', e);
      await interaction.editReply(`Sorry, I couldn't find the song at ${searchTerm}`);
      return;
    }

    if (response.status !== 'success') {
      await interaction.editReply(`Sorry, I couldn't find the song at ${searchTerm}`);
      return;
    }

    try {
      const buttonRows = this.buildButtons(response);
      await interaction.editReply({
        content: `You searched for ${searchTerm}`,
        components: buttonRows,
        embeds: [
          {
            title: `${response.data.item.name} by ${this.songwhipService.getArtists(response)}`,
            image: {
              url: response.data.item.image,
            },
          },
        ],
      });
    } catch (e) {
      Sentry.captureException(e);
      console.error('Error replying to interaction', e);
    }
  }

  private createLink(label: string, link: string): ButtonBuilder {
    return new ButtonBuilder().setStyle(ButtonStyle.Link).setLabel(label).setURL(link);
  }

  private buildButtons(songwhip: SongwhipDto): ActionRowBuilder<ButtonBuilder>[] {
    const priorityServices: string[] = ['spotify', 'itunes', 'youtube', 'deezer', 'amazon'];
    const songLinks: SongLink[] = this.songwhipService.getSongLinks(songwhip.data.item.links);

    const rows: ActionRowBuilder<ButtonBuilder>[] = [];
    const buttons: ButtonBuilder[] = [];

    // Add priority services first.
    priorityServices.forEach((serviceName: string) => {
      const songLink = songLinks.find((songLink) => songLink.serviceName === serviceName);
      if (songLink) {
        buttons.push(this.createLink(songLink.displayName, songLink.link));
      }
    });

    // Add remaining services.
    songLinks.forEach((songLink: SongLink) => {
      if (!priorityServices.includes(songLink.serviceName)) {
        buttons.push(this.createLink(songLink.displayName, songLink.link));
      }
    });

    // Split buttons into rows of 5.
    for (let i = 0; i < buttons.length; i += 5) {
      rows.push(new ActionRowBuilder<ButtonBuilder>().addComponents(...buttons.slice(i, i + 5)));
    }

    return rows;
  }
}
