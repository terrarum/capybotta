import BirthdayUpdateService from '../../src/cron/birthday-update.service';
import QueryService from '../../src/command/birthday/query.service';
import EnvVarService from '../../src/service/envvar.service';
import { Client, TextChannel } from 'discord.js';
import { DateTime } from 'luxon';
import Birthday from '../../src/model/birthday';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import { LibSQLDatabase } from 'drizzle-orm/libsql';

vi.mock('../../src/command/birthday/query.service');
vi.mock('../../src/service/envvar.service');
vi.mock("discord.js");

describe('BirthdayUpdateService', () => {
  let queryService: QueryService;
  let envVarService: EnvVarService;
  let discordClient: Client;
  let birthdayUpdateService: BirthdayUpdateService;

  beforeEach(() => {
    queryService = new QueryService({} as unknown as LibSQLDatabase);
    envVarService = new EnvVarService();
    discordClient = {
      channels: {
        fetch: vi.fn(),
      },
    } as unknown as Client;
    birthdayUpdateService = new BirthdayUpdateService(queryService, envVarService, discordClient);
  });

  it('should send a message for each birthday', async () => {
    const sixAmInMelbourne = DateTime.fromObject({
        month: 1,
        day: 1,
        hour: 6,
        minute: 0,
        second: 0,
      },
      {zone: 'Australia/Melbourne'}
    );

    const birthdays: Birthday[] = [
      {discord_id: '1', day: 1, month: 1, year: null, timezone: 'Australia/Melbourne'},
      {discord_id: '2', day: 1, month: 1, year: null, timezone: 'Australia/Melbourne'},
      {discord_id: '3', day: 1, month: 1, year: null, timezone: 'Australia/Melbourne'},
    ];

    const mockChannel = {
      send: vi.fn(),
    } as unknown as TextChannel;

    discordClient.channels.fetch.mockReturnValue(Promise.resolve(mockChannel));
    queryService.getBirthdaysByDayAndMonth.mockReturnValue(Promise.resolve(birthdays));

    await birthdayUpdateService.update(sixAmInMelbourne);

    expect(discordClient.channels.fetch).toHaveBeenCalled();
    expect(discordClient.channels.fetch).toHaveBeenCalledWith(envVarService.getBirthdayChannelId());
    expect(mockChannel.send).toHaveBeenCalledTimes(3);
    expect(mockChannel.send).toHaveBeenCalledWith('Happy birthday <@1>!');
    expect(mockChannel.send).toHaveBeenCalledWith('Happy birthday <@2>!');
    expect(mockChannel.send).toHaveBeenCalledWith('Happy birthday <@3>!');
  });
});
