import * as Sentry from '@sentry/node';
import { ChatInputCommandInteraction, Client, Events, Interaction } from 'discord.js';
import BirthdayService from './command/birthday/birthday.service';
import QueryService from './command/birthday/query.service';
import SongwhipService from './command/songwhip/songwhip.service.js';
import Command from './interface/command.js';
import CommandService from './service/command.service.js';
import DiscordService from './service/discord.service.js';
import EnvVarService from './service/envvar.service.js';
import FetchService from './utils/fetch.service.js';
import { drizzle, LibSQLDatabase } from 'drizzle-orm/libsql';

const envVarService: EnvVarService = new EnvVarService();

Sentry.init({
  dsn: envVarService.getSentryDsn(),
  tracesSampleRate: 1.0,
  profilesSampleRate: 1.0,
});

const databaseClient: LibSQLDatabase = drizzle({ connection: {
  url: envVarService.getTursoConnectionUrl(),
  authToken: envVarService.getTursoAuthToken(),
}});

const fetchService: FetchService = new FetchService();
const songwhipService: SongwhipService = new SongwhipService(fetchService);
const birthdayQueryService: QueryService = new QueryService(databaseClient);
const birthdayService: BirthdayService = new BirthdayService(envVarService, birthdayQueryService);
const commandService: CommandService = new CommandService(envVarService, songwhipService, birthdayService);
const discordService: DiscordService = new DiscordService(envVarService, commandService);

async function init() {
  // Register commands.
  const commands: Command[] = commandService.getCommands();
  await commandService.registerCommands(commands);

  // Create Discord Client.
  const discordClient: Client = await discordService.init();

  // Listen for command messages.
  discordClient.on(Events.InteractionCreate, async (interaction: Interaction) => {
    if (interaction.isModalSubmit()) {
      if (interaction.customId === 'birthdaySetModal') {
        const birthdaySetMessage: string = await birthdayService.set(interaction);
        await interaction.reply(birthdaySetMessage);
      }
    }

    if (interaction.isCommand()) {
      const commandInteraction: ChatInputCommandInteraction = interaction as ChatInputCommandInteraction;

      await commands
        .find((command: Command) => command.data.name === commandInteraction.commandName)
        ?.execute(commandInteraction);
    }
  });
}

init().then();
