import { Client, TextChannel } from 'discord.js';
import { DateTime } from 'luxon';
import QueryService from '../command/birthday/query.service';
import Birthday from '../model/birthday';
import EnvVarService from '../service/envvar.service';
import TimeCheck from './utils/time-check';

export default class BirthdayUpdateService {
  private queryService: QueryService;
  private envVarService: EnvVarService;
  private discordClient: Client;

  constructor(queryService: QueryService, envVarService: EnvVarService, discordClient: Client) {
    this.queryService = queryService;
    this.envVarService = envVarService;
    this.discordClient = discordClient;
  }

  private async sendBirthdayMessage(birthday: Birthday) {
    const channel: TextChannel = (await this.discordClient.channels.fetch(
      this.envVarService.getBirthdayChannelId(),
    )) as TextChannel;
    await channel.send(`Happy birthday <@${birthday.discord_id}>!`);
  }

  public async update(now: DateTime) {
    const timeInMelbourne: DateTime = now.setZone('Australia/Melbourne');

    const todaysBirthdays: Birthday[] = await this.queryService.getBirthdaysByDayAndMonth(
      timeInMelbourne.day,
      timeInMelbourne.month,
    );

    for (const birthday of todaysBirthdays) {
      await this.sendBirthdayMessage(birthday);
    }
  }
}
